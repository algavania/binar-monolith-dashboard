const { Article } = require('../models');
const { User } = require('../models');

module.exports = {
    getAll: async(req, res) => {
        try {
            const articles = await Article.findAll();
            res.status(200).json(articles);
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    getById: async(req, res) => {
        try {
            const article = await Article.findOne({
                where: { id: req.params.id }
            });
            res.status(200).json(article);
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    create: async(req, res) => {
        try {
            const article = await Article.create({
                writer: req.body.writer,
                title: req.body.title,
                body: req.body.body,
            });
            // res.status(200).json(article);
            res.redirect('/');
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    update: async(req, res) => {
        try {
            const article = await Article.update({
                    writer: req.body.writer,
                    title: req.body.title,
                    body: req.body.body,
                }, {
                    where: { id: req.params.id }
                })
                // res.status(201).json(article);
            res.redirect('/');
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    approve: async(req, res) => {
        try {
            const article = await Article.update({
                    approved: true,
                }, {
                    where: { id: req.params.id }
                })
                // res.status(201).json(article);
            console.log(article);
            res.redirect('/');
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    disapprove: async(req, res) => {
        try {
            const article = await Article.update({
                approved: false,
            }, {
                where: { id: req.params.id }
            })
            console.log(article);
            // res.status(201).json(article);
            res.redirect('/');
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    delete: async(req, res) => {
        try {
            const article = await Article.destroy({
                    where: { id: req.params.id }
                })
                // res.status(200).json(article);
            res.redirect('/');
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    indexViews: async(req, res) => {
        try {
            const articles = await Article.findAll();
            res.render('articles/index', {
                articles
            });
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    articlesViews: async(req, res) => {
        try {
            var hasLogin = req.cookies.auth == null ? false : true;
            const articles = await Article.findAll({
                where: { approved: true }
            });
            res.render('articles/articles', {
                articles,
                hasLogin
            });
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    createViews: (req, res) => {
        res.render('articles/create');
    },
    statsViews: async(req, res) => {
        const articles = await Article.findAll();
        const users = await User.findAll();
        const articleSize = articles.length;
        const userSize = users.length;
        res.render('articles/stats', {
            articleSize,
            userSize
        });
    },
    detailViews: async(req, res) => {
        try {
            const article = await Article.findOne({
                where: { id: req.params.id }
            });
            const updateViews = await Article.update({
                views: article.views + 1,
            }, {
                where: { id: req.params.id }
            });
            res.render('articles/show', { article });
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    editViews: async(req, res) => {
        try {
            const article = await Article.findOne({
                where: { id: req.params.id }
            });
            res.render('articles/edit', { article });
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    }
}