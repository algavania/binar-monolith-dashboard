const { User } = require("../models");

format = user => {
    const { id, username } = user;
    return {
        id,
        username,
        accessToken: user.generateToken()
    }
}

module.exports = {
    login: async(req, res) => {
        try {
            const user = await User.authenticate(req.body);
            const token = user.generateToken();
            res.cookie('auth', token);
            res.redirect('/');
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    register: async(req, res) => {
        try {
            const username = req.body.username.trim();
            if (username.length < 6) return res.status(422).json({ error: 'Username must be at least 6 characters' });

            const password = req.body.password;
            if (password.length < 6) return res.status(422).json({ error: 'Password must be at least 6 characters' });

            await User.register(req.body);
            res.redirect('/login');
        } catch (err) {
            console.log(err);
            res.status(422).json({ error: err });
        }
    },
    logout: (req, res) => {
        res.clearCookie("auth");
        res.redirect('/');
    },
    loginViews: (req, res) => {
        res.render("login");
    },
    registerViews: (req, res) => {
        res.render("register");
    },
};