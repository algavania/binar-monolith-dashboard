const express = require('express');
const router = express.Router();
const articleRouter = require('./router/article');
const userRouter = require('./router/user');

router.use(articleRouter);
router.use(userRouter);

module.exports = router;