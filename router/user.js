const router = require('express').Router();
const user = require('../controllers/userController');
const auth = require('../middlewares/hasLogin');

var cookieParser = require('cookie-parser')
router.use(cookieParser())

// API
router.post('/api/v1/login', user.login);
router.post('/api/v1/register', user.register);

// Views
router.get('/login', auth, user.loginViews);
router.get('/register', auth, user.registerViews);

// Logout
router.get('/logout', user.logout);

module.exports = router;