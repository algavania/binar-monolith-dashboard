const router = require('express').Router();
const article = require('../controllers/articleController');
const auth = require('../middlewares/auth');

var cookieParser = require('cookie-parser');
router.use(cookieParser());

// API
router.get('/api/v1/articles', article.getAll);
router.get('/api/v1/articles/:id', article.getById);
router.post('/api/v1/articles', article.create);
router.post('/api/v1/update/:id', article.update);
router.get('/api/v1/delete/:id', article.delete);
router.get('/api/v1/approve/:id', article.approve);
router.get('/api/v1/disapprove/:id', article.disapprove);

// Views
router.get('/', auth, article.indexViews);
router.get('/articles', article.articlesViews);
router.get('/article/:id', auth, article.detailViews);
router.get('/edit/:id', auth, article.editViews);
router.get('/post', auth, article.createViews);
router.get('/stats', auth, article.statsViews);

module.exports = router;