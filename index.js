const express = require('express');
const app = express();

const router = require('./router');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '../../'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(router);

// 500 Error Handler
app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: 'failed',
        errors: err.message
    });
});

// 404 Error Handler
app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404).json({
        status: 'failed',
        errors: 'Path not found'
    });
});

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`App is listening on port ${port}`));