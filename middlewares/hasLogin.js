const express = require('express');
const router = express.Router();

const jwt = require('jsonwebtoken');
var cookieParser = require('cookie-parser');
router.use(cookieParser());

router.use((req, res, next) => {
    var token = req.cookies.auth;

    // decode token
    if (token) {

        jwt.verify(token, 'secret', function(err, token_data) {
            if (err) {
                return res.status(403).send('Error');
            } else {
                req.user_data = token_data;
                return res.redirect('/');
            }
        });

    } else {
        console.log('No token');
        next();
    }
})

module.exports = router;